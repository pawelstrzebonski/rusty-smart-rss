## ========== (Rusty) Smart-RSS Makefile ==============
## ----------------------------------------------------
## This Makefile should support building using stardard
## Rust toolset, as well as the Nix toolset.
## ----------------------------------------------------

help:			## Show this help
	@sed -ne '/@sed/!s/## //p' Makefile

build:			## Build application using Rust toolset
	cargo build --release

install: 	## Install application using Rust toolset
	cargo install --path .

build-nix:		## Build using Nix toolset
	nix-build

install-nix: result	## Install using Nix toolset
	nix-env -i ./result

result:			## Build using Nix toolset
	nix-build

build-docs:		## Build documentation pages
	mkdocs build

public:			## Build documentation pages
	mkdocs build

serve-docs: public	## Serve documentation pages (locally)
	mkdocs serve

clean:			## Clean up directory by deleting files
	-cargo clean
	rm -rf public/

