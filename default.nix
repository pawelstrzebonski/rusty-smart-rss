{ pkgs ? import <nixpkgs> {} }:
with pkgs;

rustPlatform.buildRustPackage rec {
	name = "rustysmartrss";  
	nativeBuildInputs = [ pkg-config cmake];
	buildInputs = [
		freetype expat
		vulkan-loader vulkan-tools
		wayland wayland-protocols libxkbcommon swiftshader
	] ++ (with xorg; [
		libX11 libXcursor libXrandr libXi
	]);
	shellHook = ''
		export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${lib.makeLibraryPath buildInputs}";
	'';
	cargoSha256="1i0pdqv1rd76d02i3yi7syzg6gparhkin6v71jcqvvr7xx80992m";
	rev = "73208bad389797f711f5b140c4bd96109105c8a2";
	src = fetchFromGitLab {
		inherit rev;
		owner = "pawelstrzebonski";
		repo = "rusty-smart-rss";
		sha256 = "0032qs7ffhd6acydp6ly1j8chq3qx142cwnx82f926222ah5vyg0";
	};
	meta = with lib; {
		description = "A machine learning enhanced RSS feed manager, written in Rust.";
		homepage = "https://gitlab.com/pawelstrzebonski/rusty-smart-rss";
		license = licenses.agpl3;
		#maintainers = with maintainers; [ pawelstrzebonski ];
		platforms = platforms.linux;
	};
}
