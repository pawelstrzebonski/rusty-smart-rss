//! # GUI frontend to the Smart-RSS application
//! This module ties together all of this application and provides a
//! simple GUI to the user.
//!
//! The `iced` crate is used to implement the GUI.
//!
//! This applications shows two forms of the window, the main screen
//! and the add-feed screen.
//!
//! The main screen is the most relevant. It displays a subset of the
//! fetched RSS items with the item title, summary, and buttons to
//! open, like, and dislike the item. The items should be sorted based
//! on a score determined by the machine learning classifier module of
//! this application. There should also be buttons to update the feeds,
//! show the next set of feed items, and add a new feed.
//!
//! The add-feed screen is used to add a new RSS feed/channel to this
//! application.

use std::fs;
extern crate dirs;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

use iced::{
    button, text_input, Button, Color, Column, Element, Row, Sandbox, Settings, Text, TextInput,
};

use signal_hook::{
    consts::SIGHUP, consts::SIGINT, consts::SIGQUIT, consts::SIGTERM, iterator::Signals,
};

/// Thread-safe back-end container, local type to allow for a default implementation
struct Backend {
    backend: Arc<Mutex<libsmartrssrust::LibSmartRSS>>,
}
impl Default for Backend {
    /// Create default creator function that loads the back-end and spawns an auto-update thread
    fn default() -> Self {
        // Find/create application config data directory
        let confdir = dirs::config_dir().unwrap().join("smartrssrust");
        fs::create_dir_all(&confdir).unwrap();
        let dbfile = confdir
            .join("smartrss.bin")
            .into_os_string()
            .into_string()
            .unwrap();
        // Setup backend
        let libsmartrss = Arc::new(Mutex::new(libsmartrssrust::setup(&dbfile).unwrap()));
        // Setup a background update thread
        let autoupdate = Arc::clone(&libsmartrss);
        thread::spawn(move || loop {
            {
                let mut libsmartrss = autoupdate.lock().unwrap();
                libsmartrss.feeds_update();
            }
            thread::sleep(Duration::from_secs(600));
        });
        // Spawn automatic save thread
        let autosave = Arc::clone(&libsmartrss);
        thread::spawn(move || loop {
            {
                let mut libsmartrss = autosave.lock().unwrap();
                libsmartrss.save_db();
            }
            thread::sleep(Duration::from_secs(3600));
        });
        // Setup save-on-exit thread
        let mut signals = Signals::new(&[SIGTERM, SIGINT, SIGQUIT, SIGHUP]).unwrap();
        let finalsave = Arc::clone(&libsmartrss);
        thread::spawn(move || {
            for sig in signals.forever() {
                println!("Received signal {:?}", sig);
                let mut libsmartrss = finalsave.lock().unwrap();
                libsmartrss.save_db();
                println!("Saved backend");
            }
        });
        Backend {
            backend: libsmartrss,
        }
    }
}

/// State/data internal to the application, mostly for iced GUI, but
/// some to this applications backend and classifier code.
#[derive(Default)]
pub struct App {
    /// (Max) number of RSS items to display at one time
    items_per_page: usize,
    /// State of the update-feeds button
    update_button: button::State,
    /// State of the load-next-items button
    next_button: button::State,
    /// State of the exit-window button
    exit_button: button::State,
    /// State of the add-new-feed button
    add_button: button::State,
    /// State of the open-item buttons
    buttons_state: Vec<button::State>,
    /// State of the like-item buttons
    buttons_state_like: Vec<button::State>,
    /// State of the dislike-item buttons
    buttons_state_dislike: Vec<button::State>,
    /// Enum/state of which window/screen the application is currently in
    window: Window,
    /// Vector of RSS items being displayed
    items: Vec<libsmartrssrust::MyItem>,
    /// State of text input widget
    inputtext: text_input::State,
    /// Value of text input widget
    value: String,
    /// General immediate program status
    status: Status,
    /// Applications text classifiers
    libsmartrss: Backend,
}

/// All messages that can be triggered by buttons pressing or text input
#[derive(Debug, Clone)]
pub enum Message {
    /// Pressed update-feed button
    UpdatePressed,
    /// Pressed display-next-items button
    NextPressed,
    /// Pressed exit button
    ExitPressed,
    /// Pressed add-new-feed button
    AddPressed,
    /// Pressed an open-item button (index of item specified)
    LinkPressed(usize),
    /// Pressed a like-item button (index of item specified)
    ItemLiked(usize),
    /// Pressed a dislike-item button (index of item specified)
    ItemDisliked(usize),
    /// Typed in a string into text input widget
    InputTyped(String),
}

/// Which window/screen is currently rendered
#[derive(Debug, Clone, Copy)]
enum Window {
    /// Main window
    Main,
    /// Window for adding a new feed
    AddFeed,
}
/// By default, we render the main window
impl Default for Window {
    fn default() -> Self {
        Window::Main
    }
}

/// General application status type (for the last operation)
#[derive(Debug, Clone, Copy)]
enum Status {
    /// Last function succeeded
    Success,
    /// Last function failed
    Failure,
    /// No significant function success/failure to report
    Neutral,
}
/// By default, we define application status as neutral
impl Default for Status {
    fn default() -> Self {
        Status::Neutral
    }
}

/// We create the iced GUI application as a Sandbox type
impl Sandbox for App {
    /// Define relevant messages
    type Message = Message;

    /// Initiate the application, will setup the backend and classifiers
    fn new() -> Self {
        let items_per_page = 4;
        let mut s = Self {
            items_per_page: items_per_page,
            ..Default::default()
        };
        // Setup backend
        let items = {
            let backend = s.libsmartrss.backend.lock().unwrap();
            backend.items_get_unseen_limited(&(items_per_page as i64))
        };
        s.items = items.unwrap();
        s
    }

    /// Set the application window title
    fn title(&self) -> String {
        String::from("Smart-RSS")
    }

    /// Update state based on messages recieved
    ///
    /// Will use the user-input messages to:
    /// * Switch between windows
    /// * Send commands to database/backend (update or mark RSS items)
    /// * Load new RSS items and sort using classifier
    /// * Add new RSS feeds
    /// * etc.
    fn update(&mut self, message: Message) {
        match message {
            Message::AddPressed => match self.window {
                Window::AddFeed => {
                    //TODO: expose option for setting update period
                    let mut backend = self.libsmartrss.backend.lock().unwrap();
                    self.status = if backend.feed_add(&self.value, &3600).is_some() {
                        self.window = Window::Main;
                        self.value = "".to_string();
                        Status::Success
                    } else {
                        Status::Failure
                    }
                }
                Window::Main => {
                    self.status = Status::Neutral;
                    self.window = Window::AddFeed;
                }
            },
            Message::ExitPressed => {
                self.status = Status::Neutral;
                self.value = "".to_string();
                self.window = Window::Main;
            }
            Message::LinkPressed(i) => {
                self.status = if webbrowser::open(&self.items[i].url).is_ok() {
                    let mut backend = self.libsmartrss.backend.lock().unwrap();
                    if backend.item_mark_opened(&self.items[i].url).is_some() {
                        Status::Success
                    } else {
                        Status::Failure
                    }
                } else {
                    Status::Failure
                };
            }
            Message::UpdatePressed => {
                let mut backend = self.libsmartrss.backend.lock().unwrap();
                self.status = if backend.feeds_update().is_some() {
                    Status::Success
                } else {
                    Status::Failure
                };
            }
            Message::InputTyped(val) => self.value = val,
            Message::ItemLiked(i) => {
                let mut backend = self.libsmartrss.backend.lock().unwrap();
                self.status = if backend.item_like(&self.items[i].url).is_some() {
                    Status::Success
                } else {
                    Status::Failure
                }
            }
            Message::ItemDisliked(i) => {
                let mut backend = self.libsmartrss.backend.lock().unwrap();
                self.status = if backend.item_dislike(&self.items[i].url).is_some() {
                    Status::Success
                } else {
                    Status::Failure
                }
            }
            Message::NextPressed => {
                let mut s = true;
                let mut backend = self.libsmartrss.backend.lock().unwrap();
                for item in &self.items {
                    s = s && backend.item_mark_seen(&item.url).is_some();
                }
                self.status = if s { Status::Success } else { Status::Failure };
                let items = backend.items_get_unseen_limited(&(self.items_per_page as i64));
                self.items = items.unwrap();
            }
        }
    }

    /// Render the window with all its widget based on current application
    /// state
    fn view(&mut self) -> Element<Message> {
        let c = match self.status {
            Status::Success => Color {
                r: 0.0,
                g: 1.0,
                b: 0.0,
                a: 1.0,
            },
            Status::Failure => Color {
                r: 1.0,
                g: 0.0,
                b: 0.0,
                a: 1.0,
            },
            Status::Neutral => Color::WHITE,
        };
        //TODO: Shrink these if too large, or else limit # of rendered buttons
        // Ensure correct # of items can be instantiated
        if self.items_per_page > self.buttons_state.len() {
            self.buttons_state.append(&mut vec![
                Default::default();
                self.items_per_page - self.buttons_state.len()
            ]);
            self.buttons_state_like.append(&mut vec![
                Default::default();
                self.items_per_page
                    - self.buttons_state_like.len()
            ]);
            self.buttons_state_dislike.append(&mut vec![
                Default::default();
                self.items_per_page
                    - self.buttons_state_dislike.len()
            ]);
        }
        // Determine which layout based on window state
        match self.window {
            Window::Main => {
                let mut col = Column::new().padding(20).push(
                    Row::new()
                        .push(
                            Button::new(&mut self.update_button, Text::new("Update Feeds"))
                                .on_press(Message::UpdatePressed),
                        )
                        .push(
                            Button::new(&mut self.next_button, Text::new("Next Items"))
                                .on_press(Message::NextPressed),
                        ),
                );
                for (i, ((bs, bsl), bsd)) in self
                    .buttons_state
                    .iter_mut()
                    .zip(self.buttons_state_like.iter_mut())
                    .zip(self.buttons_state_dislike.iter_mut())
                    .enumerate()
                {
                    if i < self.items.len() {
                        col = col //TODO: truncate title/summary
                            .push(Text::new(&self.items[i].title))
                            .push(Text::new(&self.items[i].summary))
                            .push(Text::new(format!("Score: {:.5}", self.items[i].score)))
                            .push(
                                Row::new()
                                    .push(
                                        Button::new(bs, Text::new("Open Link"))
                                            .on_press(Message::LinkPressed(i)),
                                    )
                                    .push(
                                        Button::new(bsl, Text::new("Like Item"))
                                            .on_press(Message::ItemLiked(i)),
                                    )
                                    .push(
                                        Button::new(bsd, Text::new("Dislike Item"))
                                            .on_press(Message::ItemDisliked(i)),
                                    ),
                            );
                    }
                }
                col.push(
                    Row::new()
                        .push(
                            Button::new(&mut self.add_button, Text::new("Add Feed"))
                                .on_press(Message::AddPressed),
                        )
                        .push(
                            Button::new(&mut self.exit_button, Text::new("Exit").color(c))
                                .on_press(Message::ExitPressed),
                        ),
                )
                .into()
            }
            Window::AddFeed => Column::new()
                .padding(20)
                .push(Row::new().push(TextInput::new(
                    &mut self.inputtext,
                    "Feed URL...",
                    &self.value,
                    Message::InputTyped,
                )))
                .push(
                    Row::new()
                        .push(
                            Button::new(&mut self.add_button, Text::new("Add Feed"))
                                .on_press(Message::AddPressed),
                        )
                        .push(
                            Button::new(&mut self.exit_button, Text::new("Exit").color(c))
                                .on_press(Message::ExitPressed),
                        ),
                )
                .into(),
        }
    }
}

/// Function to start and run the GUI application
pub fn start() {
    App::run(Settings::default()).unwrap()
}
