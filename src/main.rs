//! # Smart-RSS - Rust Edition
//! A machine-learning enhanced RSS feed manager, written in Rust!
//! ## About
//! This is the Rust rewrite of the original [Python version](https://pawelstrzebonski.gitlab.io/smart-rss).
//! It is not a perfect clone, but it is largely an equivalent.
//!
//! Note: This is a personal learning project and should be treated as such.

mod frontend;

/// Main function (start logging and frontend)
fn main() {
    // Launch the GUI application
    frontend::start();
}
