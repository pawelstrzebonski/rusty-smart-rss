# (Rusty) Smart-RSS

A machine learning enhanced RSS feed manager (written in Rust). There is also a similar [Go version](https://gitlab.com/pawelstrzebonski/smart-rss-go). It is built on the [libsmartrssrust](https://gitlab.com/pawelstrzebonski/libsmartrssrust) back-end library.

For more information about program design/implementation, please consult the [documentation pages](https://pawelstrzebonski.gitlab.io/rusty-smart-rss/).

## Screenshots

![Main window](docs/smartrssrs_main.png)

## Usage/Installation

This repository contains the Rust project files (`*.toml` files) in the top level and source code in the `src/` directory. It can be built using Rust's Cargo tool and may require installation of build dependencies from your systems package respositories. A `Makefile` is included with most of the relevant build/install commands, although build dependency installation is operating system dependent and is not included.

If you have [Nix](https://nixos.org/) then you can use the included `shell.nix` to start a shell with installed dependencies for development, or else you can use the `default.nix` script to build/install a specific version of this application. Note that `default.nix` specifies the commit to be built, so it may need to be updated to build the latest version. On certain setups the even the compiled/installed Nix binary may need to be run from within the `shell.nix` environment to ensure all of the runtime dependencies are available/loaded.
