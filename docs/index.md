# (Rusty) Smart-RSS

(Rusty) Smart-RSS is a machine learning enhanced RSS feed manager, written in Rust. This is a native GUI front-end to the [libsmartrssrust](https://gitlab.com/pawelstrzebonski/libsmartrssrust) back-end, and there is also a parallel web-server/UI project, [Rusty Smart-RSS Web](https://gitlab.com/pawelstrzebonski/rusty-smart-rss-rust).

This is by no means a complete and polished product. Rather it is a pet project to practice and learn SQL/databases and GUI.

## Screenshots

![Add-new-feed screen](smartrssrs_addfeed.png)

Screen for adding a new RSS feed

![Main screen](smartrssrs_main.png)

Main application screen
