# Dependencies

This application makes use of the following Rust crates:

* [`libsmartrssrust`](https://gitlab.com/pawelstrzebonski/libsmartrssrust): Smart-RSS back-end
* [`webbrowser`](https://github.com/amodm/webbrowser-rs): Opening links from this applications in the user's default browser
* [`iced`](https://github.com/iced-rs/iced): GUI interface
* [`dirs`](https://github.com/soc/dirs-rs): Cross-platform application configuration directory definition
