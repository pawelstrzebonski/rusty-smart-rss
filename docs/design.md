# Design Document

## Files

This application is broken into two primary files:

* `main.rs`
* `frontend.rs`

The `main.rs` does very little other than start/run the front-end.

The `frontend.rs` file implements a GUI using the `iced` crate, acting as a graphical go-between the back-end from the `libsmartrssrust` library and the user.

## Frontend

The `iced` crate is used to enable user interactivity via a GUI. As of writing, there are two screens that may be shown to the user.

The main screen is shown by default. It displays (by default) 5 of the links from the database with their title, summary text, and a predicted score value (obtained using the classifier, may be related to the probability of clicking on the link or likelihood of (dis)liking it). The links will be displayed in (predicted) order of desirability/interest to the user. Each link's GUI element will have a button to open the full article in the user's default browser, as well as a pair of buttons to mark the link as either "liked" or "disliked" within this application.

The main screen also provides buttons to update the feeds (new links will be fetched from each source, provided enough time has lapsed since the last update), add a new RSS feed source (switching screens), show the next set of new links (in the process removing the currently displayed links and marking them as viewed in the backend's database), as well as a button to exit the application.

The second screen is for adding a new RSS feed source, which simply asks for the URL of the feed source and the minimal time between feed updates (in seconds, by default 3600, i.e. 1 hour).
