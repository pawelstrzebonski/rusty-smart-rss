# TODOs

This project is by no means finished/polished. A the following is a shopping list of improvements (in no particular order):

* Better GUI (expose more application settings, nicer presentation, a different crate with fewer dependencies)
* Better handling of non-ASCII/international content
* Database cleanup (remove old entries periodically, options to remove feeds)
* General code/project improvements (tests, CI, build documentation from source, decrease build dependency count, etc)
